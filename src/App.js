import React, { useState } from "react";
import { jvcHomeownersBallot } from "./abi/abi";
import Web3 from "web3";
import Nav from "./components/Nav.js";
import "./App.css";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import {CircularProgress, Grid, Typography} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
 root: {
   "& > *": {
     margin: theme.spacing(1),
   },
 },
}));

const web3 = new Web3(Web3.givenProvider);
const contractAddress = "0xdeCef6474c95E5ef3EFD313f617Ccb126236910e";
const storageContract = new web3.eth.Contract(jvcHomeownersBallot, contractAddress);

function App() {
 const classes = useStyles();
 const [voteSubmitted, setVoteSubmitted] = useState("");
 const [yesVotes, setYesVotes] = useState(0);
 const [noVotes, setNoVotes] = useState(0);
 const [waiting, setWaiting] = useState(false);

 const getVotes = async () => {
     const postYes = await storageContract.methods.getYesVotes().call();
     setYesVotes(postYes);

     const postNo = await storageContract.methods.getNoVotes().call();
     setNoVotes(postNo);
 };

 const voteYes = async () => {
     setWaiting(true);

     const accounts = await window.ethereum.enable();
     const account = accounts[0];
     const gas = (await storageContract.methods.voteYes().estimateGas()) * 1.5;
     const post = await storageContract.methods.voteYes().send({
         from: account,
         gas,
     });

     setVoteSubmitted(post.from);
     setWaiting(false);
 };

 const voteNo = async () => {
     setWaiting(true);

     const accounts = await window.ethereum.enable();
     const account = accounts[0];
     const gas = (await storageContract.methods.voteNo().estimateGas() * 1.5);
     const post = await storageContract.methods.voteNo().send({
         from: account,
         gas,
     });

     setVoteSubmitted(post.from);
     setWaiting(false);
 };

 return (
   <div className={classes.root}>
     <Nav />
     <div className="main">
       <div className="card">
         <Typography variant="h3" gutterBottom>
             JVC Homeowners Ballot
         </Typography>

         <Typography gutterBottom>
             How do you wish to vote?
         </Typography>

         <span className="buttonSpan">
           <Button
             id="yesButton"
             className="button"
             variant="contained"
             color="primary"
             type="button"
             onClick={voteYes}>Vote Yes</Button>
           <div className="divider"/>
           <Button
             id="noButton"
             className="button"
             color="secondary"
             variant="contained"
             type="button"
             onClick={voteNo}>Vote No</Button>
           <div className="divider"/>
         </span>

         {waiting && (
           <div>
               <CircularProgress />
               <Typography gutterBottom>
                   Submitting Vote ... please wait
               </Typography>
           </div>
         )}

         {!waiting && voteSubmitted && (
           <Typography gutterBottom>
               Vote Submitted: {voteSubmitted}
           </Typography>
         )}

         <span className="buttonSpan">
            <Button
                id="getVotesButton"
                className="button"
                color="default"
                variant="contained"
                type="button"
                onClick={getVotes}>Get Votes</Button>
         </span>

         {(yesVotes > 0 || noVotes > 0) && (
           <div>
           <Typography variant="h5" gutterBottom>
               Current Results
           </Typography>

           <Grid container spacing={1}>
               <Grid item xs={6}>
                   <div className="resultsAnswer resultsHeader">Vote</div>
               </Grid>
               <Grid item xs={6}>
                   <div className="resultsValue resultsHeader"># of Votes</div>
               </Grid>
               <Grid item xs={6}>
                   <div className="resultsAnswer">Yes</div>
               </Grid>
               <Grid item xs={6}>
                   <div className="resultsValue">{yesVotes}</div>
               </Grid>
               <Grid item xs={6}>
                   <div className="resultsAnswer">No</div>
               </Grid>
               <Grid item xs={6}>
                   <div className="resultsValue">{noVotes}</div>
               </Grid>
             </Grid>
           </div>
         )}
       </div>
     </div>
   </div>
 );
}

export default App;
