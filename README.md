# `jvc-homeowners-ballot-client` Repository

> The `jvc-homeowners-ballot-client` repository is simple web3 Dapp example built upon [React](https://reactjs.org/) and 
> the [web3](https://web3js.readthedocs.io/en/v1.3.0/) and [MetaMask](https://metamask.io/) dependencies - along with the smart 
> contract established in the [`jvc-homeowners-ballot`](https://gitlab.com/johnjvester/jvc-homeowners-ballot) repository.

![Dapp Example Flow](ConsenSysDappFlow.png)


## Publications

This repository is related to a DZone.com publication:

* [Moving From Full-Stack Developer To Web3 Pioneer](https://dzone.com/articles/moving-from-full-stack-developer-to-web3-pioneer)

To read more of my publications, please review one of the following URLs:

* https://dzone.com/users/1224939/johnjvester.html
* https://johnjvester.gitlab.io/dZoneStatistics/WebContent/#/stats?id=1224939


## Using This Repository

At a high level, the following steps are required to utilize this repository:

1. Migrate the smart contract within the `jvc-homeowners-ballot` repository
2. Update the `App.js` and the `contractAddress` with the correct value
3. `npm install` or `npm ci`
4. `yarn start` 

Below, is a short demonstration of the React app in action:

![Demonstration](JvcBallotAnimatedGif.gif)


## Additional Information

Made with <span style="color:red;">♥</span> &nbsp;by johnjvester@gmail.com, because I enjoy writing code.
